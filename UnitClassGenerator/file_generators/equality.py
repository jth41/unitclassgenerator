class equality():
    def create(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):
        # open file
        f = open(name+"/"+name+"Equality.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")

        f.write("\n\n\t/// <summary>delegate that defines the form of</summary>")
        f.write('\n\t/// <param name="' + name.lower() + '1"></param>')
        f.write('\n\t/// <param name="' + name.lower() + '2"></param>')
        f.write('\n\t/// <returns></returns>')
        f.write("\n\tpublic delegate bool " + name + "EqualityStrategy (" + name + " " + name.lower() + "1, " + name + " " + name.lower() + "2);")



        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")
        f.write("\n\t\t/// <summary> value comparison, checks whether the two are equal within a passed accepted equality deviation </summary>")
        f.write("\n\t\tpublic bool EqualsWithinDeviationConstant(" + name + " " + name.lower() + ", " + name + " passedAcceptedEqualityDeviationDistance)")
        f.write("\n\t\t{")
        f.write("\n\t\t\treturn (Math.Abs(")
        f.write("\n\t\t\t\t(this.GetValue(this._internalUnitType)")
        f.write("\n\t\t\t\t- ((" + name + ")(" + name.lower() + ")).GetValue(this._internalUnitType))")
        f.write("\n\t\t\t\t))")
        f.write("\n\t\t\t\t<= passedAcceptedEqualityDeviationDistance.GetValue(_internalUnitType);")
        f.write("\n\t\t}")

        f.write("\n\n\t\t/// <summary> value comparison, checks whether the two are equal within a passed accepted equality percentage </summary>")
        f.write("\n\t\tpublic bool EqualsWithinDeviationPercentage(" + name + " " + name.lower() + ", " + name + " passedAcceptedEqualityDeviationPercentage)")
        f.write("\n\t\t{")
        f.write("\n\t\t\treturn (Math.Abs(this.GetValue(this.InternalUnitType) - (" + name.lower() + ").GetValue(this.InternalUnitType))) <= this.GetValue(this.InternalUnitType);")
        f.write("\n\t\t}")

        f.write("\n\n\t\t/// <summary> value comparison, checks whether the two are equal within a passed accepted equality percentage </summary>")
        f.write("\n\t\tpublic bool EqualsWithinDistanceEqualityStrategy(" + name + " " + name.lower() + ", " + name + "EqualityStrategy passedStrategy)")
        f.write("\n\t\t{")
        f.write("\n\t\t\treturn passedStrategy(this, " + name.lower() + ");")
        f.write("\n\t\t}")

        f.write("\n\t}")

        f.write("\n\n\t/// <summary> Default deviations allowed when comparing " + name + " objects </summary>")
        f.write("\n\tpublic static partial class " + name + "DeviationDefaults")
        f.write("\n\t{")
    
        f.write("\n\n\t\t/// <summary> When comparing two " + name.lower() + " and deviation is allowed to be within a specific constant. This is that default constant </summary>")
        f.write("\n\t\tpublic static " + name + " AcceptedEqualityDeviationDistance")
        f.write("\n\t\t{")
        f.write("\n\t\t\t// TODO: This is auto-generated, based on the first unit passed. It should probably be changed.")
        f.write("\n\t\t\tget { return new " + name + "(" + name + "Type." + units[0] + ", 1); }")
        f.write("\n\t\t}")

        f.write("\n\n\t\t/// <summary> When comparing two " + name.lower() + " and deviation is allowed to be within a percentage of the first" + name + ". This is that percentage </summary>")
        f.write("\n\t\tpublic static double " + " " + name + "AcceptedEqualityDeviationDistancePercentage")
        f.write("\n\t\t{")
        f.write("\n\t\t\t// TODO: This is auto-generated. It might should be changed.")
        f.write("\n\t\t\tget { return 0.00001; }")
        f.write("\n\t\t}")

        f.write("\n\t}")

        f.write("\n\n\t/// <summary> functions that can be used for a " +  name.lower() + " object's equals functions </summary>")
        f.write("\n\tpublic static class " + name + "EqualityStrategyImplementations")
        f.write("\n\t{")
    
        f.write("\n\n\t\t/// <summary> " + name + "s are equal if they differ by less than a percentage of the first " + name + " </summary>")
        f.write('\n\t\t/// <param name="' + name.lower() + '1">first ' + name.lower() + ' being compared</param>')
        f.write('\n\t\t/// <param name="' + name.lower() + '2">second ' + name.lower() + ' being compared</param>')
        f.write("\n\t\t/// <returns></returns>")
        f.write("\n\t\tpublic static bool DefaultPercentageEquality (" + name + " " + name.lower() + "1, " + name + " " + name.lower() + "2)")
        f.write("\n\t\t{")
        f.write("\n\t\t\treturn (Math.Abs(" + name.lower() + "1.GetValue(" + name.lower() + "1.InternalUnitType) - (" + name.lower() + "2).GetValue(" + name.lower() + "1.InternalUnitType))) <= Math.Abs(" + name.lower() +"1.GetValue( " + name.lower() + "1.InternalUnitType) * " + name + "DeviationDefaults" + "." + name + "AcceptedEqualityDeviationDistancePercentage);")
        f.write("\n\t\t}")


        f.write("\n\n\t\t/// <summary> " + name + "s are equal if there values are within the passed deviation constant. If they are not within the constant </summary>")
        f.write('\n\t\t/// <param name="' + name.lower() + '1">first ' + name.lower() + ' being compared</param>')
        f.write('\n\t\t/// <param name="' + name.lower() + '2">second ' + name.lower() + ' being compared</param>')
        f.write("\n\t\t/// <returns></returns>")
        f.write("\n\t\tpublic static bool DefaultConstantEquality (" + name + " " + name.lower() + "1, " + name + " " + name.lower() + "2)")
        f.write("\n\t\t{")
        f.write("\n\t\t\treturn (Math.Abs(" + name.lower() + "1.GetValue(" + name.lower() + "1.InternalUnitType) - (" + name.lower() + "2).GetValue(" + name.lower() + "1.InternalUnitType))) <= " + name + "DeviationDefaults" + ".AcceptedEqualityDeviationDistance.GetValue(" + name.lower() + "1.InternalUnitType);")
        f.write("\n\t\t}")

        f.write("\n\t}")
   

        f.write("\n}")


