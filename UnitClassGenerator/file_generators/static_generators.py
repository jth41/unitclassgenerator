class static_generators():
    def create(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):
        # open file
        f = open(name+"/"+name+"StaticGenerators.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")

        for unit, unit_plural in zip(units, units_plural):
            f.write("\n\n\t\t///<summary>Generator method that constructs " + name + " with assumption that the passed value is in " + unit_plural + "</summary>")
            f.write('\n\t\t///<param name="passedValue"></param>')
            f.write("\n\t\t///<returns></returns>")
            f.write("\n\t\tpublic static " + name + " " + "Make" + name + "With" + unit_plural + "(double passedValue)")
            f.write("\n\t\t{")
            f.write("\n\t\t\treturn new " + name + "(" + name + "Type." + unit + ", passedValue);")
            f.write("\n\t\t}")

        f.write("\n\t}")
        f.write("\n}")



