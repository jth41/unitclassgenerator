class icomparable():
    def create(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):
        # open file
        f = open(name+"/"+name+"IComparable.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name + " : IComparable, IComparable<" + name + " >")
        f.write("\n\t{")

        # functions
        f.write("\n\t\t/// <summary> This implements the IComparable (" + name + ") interface and allows " + name + "s to be sorted and such </summary>")
        f.write("\n\t\tpublic int CompareTo(" + name + " other)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tif (this.Equals(other))")
        f.write("\n\t\t\t{")
        f.write("\n\t\t\t\treturn 0;")
        f.write("\n\t\t\t}")
        f.write("\n\t\t\telse")
        f.write("\n\t\t\t{")
        f.write("\n\t\t\t\treturn _intrinsicValue.CompareTo(other.GetValue(_internalUnitType));")
        f.write("\n\t\t\t}")
        f.write("\n\t\t}")

        f.write("\n\n\t\t/// <summary> This implements the IComparable (" + name + ") interface and allows " + name + "s to be sorted and such </summary>")
        f.write("\n\t\tpublic int CompareTo(object obj)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tif (obj == null)")
        f.write("\n\t\t\t{")
        f.write('\n\t\t\t\tthrow new ArgumentNullException("obj");')
        f.write("\n\t\t\t}")
        f.write("\n\n\t\t\tif (!( obj is " + name + "))")
        f.write("\n\t\t\t{")
        f.write('\n\t\t\t\tthrow new ArgumentException("Expected type ' + name + '.", "obj");')
        f.write("\n\t\t\t}")
        f.write("\n\n\t\t\treturn this.CompareTo((" + name + ")obj);")
        f.write("\n\t\t}")

        f.write("\n\t}")
        f.write("\n}")

    def create_composite(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):
        # open file
        f = open(name+"/"+name+"IComparable.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name + " : IComparable, IComparable<" + name + " >")
        f.write("\n\t{")

        # functions
        f.write("\n\t\t/// <summary> This implements the IComparable (" + name + ") interface and allows " + name + "s to be sorted and such </summary>")
        f.write("\n\t\tpublic int CompareTo(" + name + " other)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tif (this.Equals(other))")
        f.write("\n\t\t\t{")
        f.write("\n\t\t\t\treturn 0;")
        f.write("\n\t\t\t}")
        f.write("\n\t\t\telse")
        f.write("\n\t\t\t{")
        f.write("\n\t\t\t\treturn this." + units_plural[1] + ".CompareTo(other.GetValue(" + name + "Type." + units[1] + "));")
        f.write("\n\t\t\t}")
        f.write("\n\t\t}")

        f.write("\n\n\t\t/// <summary> This implements the IComparable (" + name + ") interface and allows " + name + "s to be sorted and such </summary>")
        f.write("\n\t\tpublic int CompareTo(object obj)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tif (obj == null)")
        f.write("\n\t\t\t{")
        f.write('\n\t\t\t\tthrow new ArgumentNullException("obj");')
        f.write("\n\t\t\t}")
        f.write("\n\n\t\t\tif (!( obj is " + name + "))")
        f.write("\n\t\t\t{")
        f.write('\n\t\t\t\tthrow new ArgumentException("Expected type ' + name + '.", "obj");')
        f.write("\n\t\t\t}")
        f.write("\n\n\t\t\treturn this.CompareTo((" + name + ")obj);")
        f.write("\n\t\t}")

        f.write("\n\t}")
        f.write("\n}")



