class conversion():
    def create(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):
        # open file
        f = open(name+"/"+name+"Conversion.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")

        # ConvertDistance()
        f.write("\n\t\t/// <summary>Converts one unit of " + name + " to another</summary>")
        f.write('\n\t\t/// <param name="typeConvertingTo">input unit type</param>')
        f.write('\n\t\t/// <param name="passedValue"></param>')
        f.write('\n\t\t/// <param name="typeConvertingFrom">desired output unit type</param>')
        f.write('\n\t\t/// <returns>passedValue in desired units</returns>')
        f.write("\n\t\tpublic static double Convert" + name + "(" + name + "Type typeConvertingFrom, double passedValue, " + name + "Type typeConvertingTo)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tdouble returnDouble = 0.0;")
        f.write("\n\n\t\t\tswitch (typeConvertingFrom)")
        f.write("\n\t\t\t{")

        for unit in units:
                f.write("\n\t\t\t\tcase " + name + "Type." + unit + ":")
                f.write("\n\t\t\t\t\tswitch (typeConvertingTo)")
                f.write("\n\t\t\t\t\t{")
                i =0
                for subunit in units:
                    
                    f.write("\n\t\t\t\t\t\tcase " + name + "Type." + subunit + ":")
                    if subunit == unit:
                        f.write("\n\t\t\t\t\t\t\treturnDouble = passedValue; // Return passed in " + unit)
                    else:
                        f.write("\n\t\t\t\t\t\t\treturnDouble = passedValue " + "* " + str(conversion_factors[i]) + "; // Convert " + unit + " to " + subunit)
                    f.write("\n\t\t\t\t\t\t\tbreak;")
                    i = i+1


                f.write("\n\t\t\t\t\t}\n\t\t\t\t\tbreak;")

        f.write("\n\t\t\t}")

        f.write("\n\t\t\treturn returnDouble;")

        f.write("\n\t\t}")

    
        f.write("\n\t}")
        f.write("\n}")


