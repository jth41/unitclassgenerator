import re

class tests():
    def create(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):

        # open file
        f = open(name+"/"+name+"Tests.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("using NUnit.Framework;")
        f.write("using UnitClassLibrary;")
        f.write("using FluentAssertions;")
        f.write("\n\n namespace UnitLibraryTests")
        f.write("\n{")
        f.write("\n\n\t[TestFixture()]")
        f.write("\n\tpublic class " + name + "Tests")
        f.write("\n\t{")

        #Test
        f.write("\n\n")
        f.write("\n\t\t[Test()]")
        f.write("\n\t\tpublic void " + name + "_SelfConversionTest()")
        f.write("\n\t\t{")
        f.write("\n\t\t\t" + name + "  testInstance = new " + name + "(" + name + "Type." + units[1] + ", 1);")
        f.write("\n\t\t\ttestInstance." + units_plural[1] + ".Should().Be(1);")
        f.write("\n\t\t}")

        f.write("\n\n\t}")
        f.write("\n}")
 