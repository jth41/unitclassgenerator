import re

class properties():
    def create(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):
        # open file
        f = open(name+"/"+name+"Properties.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")

        for unit, unit_plural in zip(units, units_plural):
            f.write("\n\t\tpublic double " + unit_plural)
            f.write("\n\t\t{")
            f.write("\n\t\t\tget { return _retrieveIntrinsicValueAsDesiredExternalUnit(" + name + "Type." + unit + "); }")
            f.write("\n\t\t}")

        f.write("\n\n\t\tpublic double GetValue(" + name + "Type Units)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tswitch (Units)")
        f.write("\n\t\t\t{")

        for unit, unit_plural in zip(units, units_plural):
            f.write("\n\t\t\t\tcase " + name + "Type." + unit + ":")
            f.write("\n\t\t\t\t\treturn " + unit_plural + ";")

        f.write("\n\t\t\t}")
        f.write('\n\t\t\tthrow new Exception("Unknown ' + name + 'Type");')
        f.write("\n\t\t}")



        f.write("\n\t}")
        f.write("\n}")

    def create_composite(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors, name_1, name_2):
        # open file
        f = open(name+"/"+name+"Properties.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")

        for unit, unit_plural in zip(units, units_plural):
            f.write("\n\t\tpublic double " + unit_plural)
            f.write("\n\t\t{")
            #f.write("\n\t\t\tget { return _retrieveIntrinsicValueAsDesiredExternalUnit(" + name + "Type." + unit + "); }")
            f.write("\n\t\t\tget { return _" + name_1.lower() + "." + [a for a in re.split(r'([A-Z][a-z]*)', unit_plural) if a][0] + " * _" + name_2.lower() + "." + [a for a in re.split(r'([A-Z][a-z]*)', unit_plural) if a][1] + "; }")
            f.write("\n\t\t}")

        f.write("\n\n\t\tpublic double GetValue(" + name + "Type Units)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tswitch (Units)")
        f.write("\n\t\t\t{")

        for unit, unit_plural in zip(units, units_plural):
            f.write("\n\t\t\t\tcase " + name + "Type." + unit + ":")
            f.write("\n\t\t\t\t\treturn " + unit_plural + ";")

        f.write("\n\t\t\t}")
        f.write('\n\t\t\tthrow new Exception("Unknown ' + name + 'Type");')
        f.write("\n\t\t}")



        f.write("\n\t}")
        f.write("\n}")

    def create_composite_divide(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors, name_1, name_2):
        # open file
        f = open(name+"/"+name+"Properties.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")

        for unit, unit_plural in zip(units, units_plural):
            f.write("\n\t\tpublic double " + unit_plural)
            f.write("\n\t\t{")
            #f.write("\n\t\t\tget { return _retrieveIntrinsicValueAsDesiredExternalUnit(" + name + "Type." + unit + "); }")
            f.write("\n\t\t\tget { return _" + name_1.lower() + "." + [a for a in re.split(r'([A-Z][a-z]*)', unit_plural) if a][0] + " / _" + name_2.lower() + "." + [a for a in re.split(r'([A-Z][a-z]*)', unit_plural) if a][2] + "; }")
            f.write("\n\t\t}")

        f.write("\n\n\t\tpublic double GetValue(" + name + "Type Units)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tswitch (Units)")
        f.write("\n\t\t\t{")

        for unit, unit_plural in zip(units, units_plural):
            f.write("\n\t\t\t\tcase " + name + "Type." + unit + ":")
            f.write("\n\t\t\t\t\treturn " + unit_plural + ";")

        f.write("\n\t\t\t}")
        f.write('\n\t\t\tthrow new Exception("Unknown ' + name + 'Type");')
        f.write("\n\t\t}")



        f.write("\n\t}")
        f.write("\n}")


