class constants():
    def create(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):
        # open file
        f = open(name+"/"+name+"Constants.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")

        for unit in units:
            f.write("\n\n\t\tpublic static " + name + " " + unit)
            f.write("\n\t\t{")
            f.write("\n\t\t\tget { return new " + name + "(" + name + "Type." + unit + ", 1); }")
            f.write("\n\t\t}")

        f.write("\n\t}")
        f.write("\n}")


