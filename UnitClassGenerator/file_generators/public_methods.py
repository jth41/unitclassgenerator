class public_methods():
    def create(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):
        # open file
        f = open(name+"/"+name+"PublicMethods.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")

        # ToString()
        f.write("\n\n\t/// <summary>prints the value and unit type converted to</summary>")
        f.write('\n\t/// <param name="' + name.lower() + 'Type"></param>')
        f.write("\n\tpublic string ToString(" + name + "Type " + name.lower() + "Type)")
        f.write("\n\t{")
        f.write('\n\t\treturn this.GetValue(' + name.lower() + 'Type) + " " + ' + name.lower() + 'Type;')
        f.write("\n\t}")

        # Negate()
        f.write("\n\n\t/// <summary>Creates a new object that is the negative of this</summary>")
        f.write("<returns>new object with value equivalent to result</returns>")
        f.write("\n\tpublic " + name + " Negate()")
        f.write("\n\t{")
        f.write("\n\t\treturn new " + name + "(_internalUnitType, _intrinsicValue * -1);")
        f.write("\n\t}")

        # AbsoluteValue()
        f.write("\n\n\t/// <summary>Creates a new object that is the absolute value of this</summary>")
        f.write("<returns>new object with value equivalent to result</returns>")
        f.write("\n\tpublic " + name + " AbsoluteValue()")
        f.write("\n\t{")
        f.write("\n\t\treturn new " + name + "(_internalUnitType, Math.Abs(_intrinsicValue));")
        f.write("\n\t}")

        # RaiseToPower()
        f.write("\n\n\t/// <summary> multiplies itself a given number of times</summary>")
        f.write("<returns>new object with value equivalent to result</returns>")
        f.write("\n\tpublic " + name + " RaiseToPower(double power)")
        f.write("\n\t{")
        f.write("\n\t\treturn this ^ power;")
        f.write("\n\t}")

        f.write("\n\t}")
        f.write("\n}")

    def create_composite(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors, name_1, name_2):
        # open file
        f = open(name+"/"+name+"PublicMethods.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")

        # ToString()
        f.write("\n\n\t/// <summary>prints the value and unit type converted to</summary>")
        f.write('\n\t/// <param name="' + name.lower() + 'Type"></param>')
        f.write("\n\tpublic string ToString(" + name + "Type " + name.lower() + "Type)")
        f.write("\n\t{")
        f.write('\n\t\treturn this.GetValue(' + name.lower() + 'Type) + " " + ' + name.lower() + 'Type;')
        f.write("\n\t}")

        # Negate()
        f.write("\n\n\t/// <summary>Creates a new object that is the negative of this</summary>")
        f.write("<returns>new object with value equivalent to result</returns>")
        f.write("\n\tpublic " + name + " Negate()")
        f.write("\n\t{")
        f.write("\n\t\treturn new " + name + "(_" + name_1.lower() + " *1, _" + name_2.lower() + "* -1);")
        f.write("\n\t}")

        # AbsoluteValue()
        f.write("\n\n\t/// <summary>Creates a new object that is the absolute value of this</summary>")
        f.write("<returns>new object with value equivalent to result</returns>")
        f.write("\n\tpublic " + name + " AbsoluteValue()")
        f.write("\n\t{")
        f.write("\n\t\treturn new " + name + "(_" + name_1.lower() + " *-1, _" + name_2.lower() + "* -1);")
        f.write("\n\t}")

        # RaiseToPower()
        f.write("\n\n\t/// <summary> multiplies itself a given number of times</summary>")
        f.write("<returns>new object with value equivalent to result</returns>")
        f.write("\n\tpublic " + name + " RaiseToPower(double power)")
        f.write("\n\t{")
        f.write("\n\t\treturn this ^ power;")
        f.write("\n\t}")

        f.write("\n\t}")
        f.write("\n}")
