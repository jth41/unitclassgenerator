class enums():
    def create(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):
        # open file
        f = open(name+"/"+name+"Enums.cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\t/// <summary> Enum for specifying the type of unit a " + name + " is.</summary>")
        f.write("\n\tpublic enum " + name + "Type { " + ', '.join(units) + " }")

        f.write("\n}")


