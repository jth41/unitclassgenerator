import re

class main_file():
    def create(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors):

        # open file
        f = open(name+"/"+name+".cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")

        # region: fields and internal properties
        f.write("\n\t\t#region _fields and Internal Properties")
        f.write("\n\n\t\tinternal " + name + "Type InternalUnitType")
        f.write("\n\t\t{")
        f.write("\n\t\t\tget { return _internalUnitType; }")
        f.write("\n\t\t}")
        f.write("\n\t\tprivate " + name + "Type _internalUnitType;")
        f.write("\n\n\t\tprivate double _intrinsicValue;");
        f.write("\n\n\t\tpublic " + name + "EqualityStrategy EqualityStrategy");
        f.write("\n\t\t{")
        f.write("\n\t\t\tget { return _equalityStrategy; }");
        f.write("\n\t\t\tset { _equalityStrategy = value; }");
        f.write("\n\t\t}")
        f.write("\n\n\t\tprivate " + name + "EqualityStrategy _equalityStrategy;")
        f.write("\n\n\t\t#endregion")

        # region: constructors
        f.write("\n\n\t\t#region Constructors")

        # Zero Constructor
        f.write("\n\n\t\t/// <summary> Zero Constructor </summary>")
        f.write("\n\t\t public " + name + "(" + name + "EqualityStrategy passedStrategy = null)")
        f.write("\n\t\t{")
        f.write("\n\t\t\t_intrinsicValue = 0;")
        f.write("\n\t\t\t_internalUnitType = " + name + "Type." + units[0] + ";")
        f.write("\n\t\t\t_intrinsicValue = 0;")
        f.write("\n\t\t}")

        # Accepts standard types for input
        f.write("\n\n\t\t/// <summary> Accepts standard types for input. </summary>")
        f.write("\n\t\tpublic " + name + "(" + name + "Type passed" + name + "Type, double passedInput, " + name + "EqualityStrategy passedStrategy = null)")
        f.write("\n\t\t{")
        f.write("\n\t\t\t_intrinsicValue = passedInput;")
        f.write("\n\t\t\t_internalUnitType = passed" + name + "Type;")
        f.write("\n\t\t\t_equalityStrategy = _chooseDefaultOrPassedStrategy(passedStrategy);")
        f.write("\n\t\t}")

        # Copy Constructor
        f.write("\n\n\t\t/// <summary> Copy constructor (new unit with same fields as the passed) </summary>")
        f.write("\n\t\tpublic " + name + "(" + name + " passed" + name + ")")
        f.write("\n\t\t{")
        f.write("\n\t\t\t_intrinsicValue = passed" + name + "._intrinsicValue;")
        f.write("\n\t\t\t_internalUnitType = passed" + name + "._internalUnitType;")
        f.write("\n\t\t\t_equalityStrategy = passed" + name + "._equalityStrategy;")
        f.write("\n\t\t}")


        f.write("\n\n\t\t#endregion")


        # region: helper methods

        f.write("\n\n\t\t#region helper _methods")
        f.write("\n\n\t\tprivate static " + name + "EqualityStrategy _chooseDefaultOrPassedStrategy(" + name + "EqualityStrategy passedStrategy)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tif (passedStrategy == null)")
        f.write("\n\t\t\t{")
        f.write("\n\t\t\t\treturn " + name + "EqualityStrategyImplementations.DefaultConstantEquality;")
        f.write("\n\t\t\t}")
        f.write("\n\t\t\telse")
        f.write("\n\t\t\t{")
        f.write("\n\t\t\t\treturn passedStrategy;")
        f.write("\n\t\t\t}")
        f.write("\n\t\t}")

        f.write("\n\n\t\tprivate double _retrieveIntrinsicValueAsDesiredExternalUnit(" + name + "Type to" + name + "Type)")
        f.write("\n\t\t{")
        f.write("\n\t\t\treturn Convert" + name + "(_internalUnitType, _intrinsicValue, to" + name + "Type);")
        f.write("\n\t\t}")
        f.write("\n\n\t\t#endregion")

        f.write("\n\t}")
        f.write("\n}")

    def create_composite(name, units, units_plural, enums, enums_plural, special_constants, conversion_factors, name_1, name_2, old_units, old_units_2):

        # open file
        f = open(name+"/"+name+".cs", 'w')

        # file opening
        f.write("using System;")
        f.write("\n\n namespace UnitClassLibrary")
        f.write("\n{")
        f.write("\n\n\tpublic partial class " + name)
        f.write("\n\t{")

        # region: fields and internal properties
        f.write("\n\t\t#region _fields and Internal Properties")
        f.write("\n\t\tprivate " + (name_1) + " _" + name_1.lower() +";")
        f.write("\n\t\tprivate " + (name_2) + " _" + name_2.lower() +";")
        f.write("\n\n\t\tinternal " + name + "Type InternalUnitType")
        f.write("\n\t\t{")
        f.write("\n\t\t\tget { return _internalUnitType; }")
        f.write("\n\t\t}")
        f.write("\n\t\tprivate " + name + "Type _internalUnitType;")
        f.write("\n\n\t\tpublic " + name + "EqualityStrategy EqualityStrategy");
        f.write("\n\t\t{")
        f.write("\n\t\t\tget { return _equalityStrategy; }");
        f.write("\n\t\t\tset { _equalityStrategy = value; }");
        f.write("\n\t\t}")
        f.write("\n\n\t\tprivate " + name + "EqualityStrategy _equalityStrategy;")
        f.write("\n\n\t\t#endregion")

        # region: constructors
        f.write("\n\n\t\t#region Constructors")

        # Zero Constructor
        f.write("\n\n\t\t/// <summary> Zero Constructor </summary>")
        f.write("\n\t\t public " + name + "(" + name + "EqualityStrategy passedStrategy = null)")
        f.write("\n\t\t{")
        f.write("\n\t\t\t_" + name_1.lower() + " = new " + name_1 + "();")
        f.write("\n\t\t\t_" + name_2.lower() + " = new " + name_2 + "();")
        f.write("\n\t\t\t_chooseDefaultOrPassedStrategy(passedStrategy);")
        f.write("\n\t\t}")

        # Construct with two unit types
        f.write("\n\n\t\t/// <summary> constructor that creates moment based on the passed units </summary>")
        f.write("\n\t\tpublic " + name + "(" + name_1 + " passed" + name_1 + ", " + name_2 + " passed" + name_2 + ", " + name + "EqualityStrategy passedStrategy = null)")
        f.write("\n\t\t{")
        f.write("\n\t\t\t_" + name_1.lower() + " = passed" + name_1 +";")
        f.write("\n\t\t\t_" + name_2.lower() + " = passed" + name_2 +";")
        f.write("\n\t\t\t_chooseDefaultOrPassedStrategy(passedStrategy);")
        f.write("\n\t\t}")


        

        # Copy Constructor
        f.write("\n\n\t\t/// <summary> Copy constructor (new unit with same fields as the passed) </summary>")
        f.write("\n\t\tpublic " + name + "(" + name + "Type passed" + name + "Type, double passedValue)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tswitch (passed" + name + "Type)")
        f.write("\n\t\t\t{")
        for unit in units:
            f.write("\n\t\t\tcase " + name + "Type." + unit + ":")
            f.write("\n\t\t\t\t_" + name_1.lower() + " = new " + name_1 + "(" + name_1 + "Type." + find_name([a for a in re.split(r'([A-Z][a-z]*)', unit) if a][0][0:-2], old_units) + ", passedValue);")
            f.write("\n\t\t\t\t_" + name_2.lower() + " = new " + name_2 + "(" + name_2 + "Type." + find_name([a for a in re.split(r'([A-Z][a-z]*)', unit) if a][-1][0:-2], old_units_2) + ", 1);")
            f.write("\n\t\t\t\tbreak;")
        f.write("\n\t\t\t}")

        f.write("\n\t\t}")


        f.write("\n\n\t\t#endregion")


        # region: helper methods

        f.write("\n\n\t\t#region helper _methods")
        f.write("\n\n\t\tprivate static " + name + "EqualityStrategy _chooseDefaultOrPassedStrategy(" + name + "EqualityStrategy passedStrategy)")
        f.write("\n\t\t{")
        f.write("\n\t\t\tif (passedStrategy == null)")
        f.write("\n\t\t\t{")
        f.write("\n\t\t\t\treturn " + name + "EqualityStrategyImplementations.DefaultConstantEquality;")
        f.write("\n\t\t\t}")
        f.write("\n\t\t\telse")
        f.write("\n\t\t\t{")
        f.write("\n\t\t\t\treturn passedStrategy;")
        f.write("\n\t\t\t}")
        f.write("\n\t\t}")
        f.write("\n\n\t\t#endregion")

        f.write("\n\t}")
        f.write("\n}")

def find_name(str, list):
    for x in list:
        if x.startswith(str):
            return x
    return ""