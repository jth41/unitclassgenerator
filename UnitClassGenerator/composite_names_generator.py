class names_generator():
    def generate(units, units_plural, units_2, units_2_plural, type):
        new_units = []
        new_units_plural = []

        if type == "divide":
            for unit, unit_plural, in zip(units, units_plural):
                for unit_2, unit_2_plural, in zip(units_2, units_2_plural):
                    new_units.append(unit_plural + "Per" + unit_2)
                    new_units_plural.append(unit_plural + "Per" + unit_2_plural)

        elif type == "multiply":
            for unit, unit_plural, in zip(units, units_plural):
                for unit_2, unit_2_plural, in zip(units_2, units_2_plural):
                    new_units.append(unit_plural + unit_2)
                    new_units_plural.append(unit_plural + unit_2_plural)

        return new_units, new_units_plural
