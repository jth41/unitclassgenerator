from tkinter import *
from tkinter import filedialog
from file_generator import *
from composite_unit_class_generator import *
from conversion_factors_wizard import *
import time


class wizard:

    def __init__(self, master):
        master.geometry ("500x500")
        self.root = master
        master.title ("Unit Class Creation Wizard")
        
        
        w = Label(master, text="Type of unit class to create: ")
        w.grid(row=0, column=0)

        global v
        v = IntVar()
        Radiobutton(master, text="Standard", variable=v, value=1).grid(row=1, column=0, sticky='w')
        Radiobutton(master, text="Composite", variable=v, value=2).grid(row=2, column=0, sticky='w')
        Radiobutton(master, text="Multi-dimensional", variable=v, value=3).grid(row=3, column=0, sticky='w')

        select_button = Button(master, text="Select", command=self.select)
        select_button.grid(row=4, column=0)
            

    def select(self):
        if (v.get()==1):
            global entry_number
            global entry_name

            Label(master, text="Name: ").grid(row=5, column=0, sticky='w')
            entry_name = Entry()
            entry_name.grid(row=5, column=1, sticky='w')
            Label(master, text="Number of units: ").grid(row=5, column=2, sticky='w')
            entry_number = Entry()
            entry_number.grid(row=5, column=3, sticky='w')


            global units_entries
            global units_entries_plural
            global units_entries_text
            global units_entries_plural_text

            units_entries = []
            units_entries_plural = []
            units_entries_text = []
            units_entries_plural_text = []


            make_unit_fields_button = Button(master, text="Make fields", command=self.units)
            make_unit_fields_button.grid(row=7, column=3)



        if (v.get()==2):
            Label(master, text="Name: ").grid(row=5, column=0, sticky='w')
            entry_name = Entry()
            entry_name.grid(row=5, column=1, sticky='w')

            Label(master, text="First Unit Properties").grid(row=6, column=0, sticky='w')
            Label(master, text="Second Unit Properties").grid(row=7, column=0, sticky='w')
            file_selector_button_1 = Button(master, text="Choose file", command=self.select_file_1)
            file_selector_button_2 = Button(master, text="Choose file", command=self.select_file_2)
            file_selector_button_1.grid(row=6, column=1, sticky='w')
            file_selector_button_2.grid(row=7, column=1, sticky='w')

            global multiply_divide_selection
            multiply_divide_selection = IntVar()
            Radiobutton(master, text="Units Multiplied", variable=multiply_divide_selection, value=1).grid(row=9, column=0, sticky='w')
            Radiobutton(master, text="Units Divided", variable=multiply_divide_selection, value=2).grid(row=9, column=1, sticky='w')

            create_composite_button = Button(master, text="Create class", command=self.create_composite)
            create_composite_button.grid(row=10, column=0, sticky='w')
            

    def standard(self):
        #first_file = filedialog.askopenfilename()

        for i in range(0, len(units_entries)):
            if units_entries[i].get() != "":
                units_entries_text.append(units_entries[i].get())
            if units_entries_plural[i].get() != "":
                units_entries_plural_text.append(units_entries_plural[i].get())
        
        conversion_factors = units_entries_text
        conversion_factors = [1]*len(conversion_factors)

        enums_list = []
        enums_list_plural = []
        special_constants = []

        for i in range(0, len(units_entries_text)):
            if (unit_check_boxes_vars[i].get() == 0):
                enums_list.append(units_entries_text[i])
                enums_list_plural.append(units_entries_plural_text[i])
        unit_check_boxes_vars[i]

        new_unit_class(entry_name.get(),units_entries_text, units_entries_plural_text, enums_list, enums_list_plural, special_constants, len(units_entries_text)*'1')
        #conversion_factors_wizard.create_window(entry_name.get(), units_entries_text, units_entries_plural_text, enums_list, enums_list_plural, special_constants)




    def select_file_1(self):
        global first_file
        first_file = filedialog.askdirectory()

    def select_file_2(self):
        global second_file
        second_file = filedialog.askdirectory()

    def create_composite(self):
        global first_file
        global second_file

        f1 = open(first_file, "r")
        f2 = open(second_file, "r")

        if (multiply_divide_selection.get()==1):
            unit_class_generator.generate(entry_name.get(), f1, f2, "multiply")
        else:
            unit_class_generator.generate(entry_name.get(), f1, f2, "divide")

    #def multiply(self):
    #    unit_class_generator.generate(entry_name.get(), f1, f2, "multiply")

    #def divide(self):
    #    unit_class_generator.generate(entry_name.get(), f1, f2, "divide")

    def units(self):
        Label(master, text="Not enum").grid(row=6, column=0)
        Label(master, text="Unit").grid(row=6, column=1)
        Label(master, text="Plural of unit").grid(row=6, column=2)

        unit_check_boxes =[]
        global unit_check_boxes_vars 
        unit_check_boxes_vars = []

        for i in range(0, int(entry_number.get())):
            units_entries.append(Entry())
            units_entries[i].grid(row=7+i, column=1)

            unit_check_boxes_vars.append(IntVar())
            unit_check_boxes.append(Checkbutton(variable=unit_check_boxes_vars[i]))
            unit_check_boxes[i].grid(row=7+i, column=0)

            units_entries_plural.append(Entry())
            units_entries_plural[i].grid(row=7+i, column=2)

        submit_button = Button(master, text="Submit", command=self.standard)

        submit_button.grid(row=20, column=3)


    
    def composite(self):
        #first_file = filedialog.askopenfilename()
        print("composite!")

    def multi_dimensional(self):
        print("multi!")


        

v = 0

master = Tk()
app = wizard(master)
def update():
    master.after(1000, update)
    master.mainloop()
update()