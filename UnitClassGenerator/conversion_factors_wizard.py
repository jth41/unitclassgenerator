from tkinter import *
from tkinter import filedialog
from file_generator import *
from composite_unit_class_generator import *

class conversion_factors_wizard(object):

    def __init__(self, main):
        global name_global
        global units_global
        global units_plural_global
        global enum_names_global
        global enums_plural_global
        global special_constants_global

        #units = ["Foot", "Inch", "Yard"]
        #units_plural = ["Feet", "Inches", "Yards"]

        main.geometry ("500x500")
        self.root = main
        main.title ("Conversion Factors")

        Label(main, text="Insert conversion factors for each below as a string.").grid(row=0, column=1)


        i = 0
        global conversion_strings
        conversion_strings = []
        for unit in units_global:
            for subunit in units_global:
                if subunit != unit:
                    i = i+1
                    conversion_text = unit + " per " + subunit
                    Label(main, text=conversion_text).grid(row=i+2, column=0)
                    conversion_strings.append(Entry(main))
                    conversion_strings[i-1].grid(row=i+2, column=1, sticky='w')

        submit_button = Button(main, text="Submit", command=self.submit)
        submit_button.grid(row=i+4, column=0, sticky='w')

    def submit(self):
        new_unit_class(name_global, units_global, units_plural_global, enum_names_global, enums_plural_global, special_constants_global, self.convert_entries_to_strings(conversion_strings))


    @staticmethod
    def convert_entries_to_strings(entries):
        strings = []
        for entry in entries:
            strings.append(entry.get())

        return strings

   
    def create_window(name, units, units_plural, enum_names, enums_plural, special_constants):
        global name_global
        global units_global
        global units_plural_global
        global enum_names_global
        global enums_plural_global
        global special_constants_global

        name_global = name
        units_global = units
        units_plural_global = units_plural
        enum_names_global = enum_names
        enums_plural_global = enums_plural
        special_constants_global = special_constants


        main = Tk()
        app = conversion_factors_wizard(main)
        main.mainloop()

#conversion_factors_wizard.create_window()
