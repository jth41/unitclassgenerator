

class unit_class_reader(): 
    # takes as input the xProperties.cs file and returns units singular plural and name 
    def read_properties(file):
        lines = file.readlines()

        name = ""
        units = []
        units_plural = []

        #for line in lines:
        for i in range(0, len(lines)):
            if "public partial class" in lines[i]:
                name = lines[i].split()[-1]
            if "case " + name + "Type." in lines[i]:
                if lines[i].split()[-1].split(".")[-1].replace(":","") is not None:
                    units.append(lines[i].split()[-1].split(".")[-1].replace(":",""))
                if lines[i+1].split()[-1].replace(";","") is not None:
                    units_plural.append(lines[i+1].split()[-1].replace(";",""))
        
        return name, units, units_plural

    # takes as input the xConversion.cs file and returns the conversion factors in order.
    def read_conversions(file):
        lines = file.readlines()

        conversion_strings = []

        for i in range(0, len(lines)):
            if "passedValue;" in lines[i]:
                for j in range(i, 0, -1):
                     if "switch" in lines[j]:
                         convert_from = lines[j-1].split(".")[1].replace(":","").rstrip('\n')
                         convert_to = lines[j-1].split(".")[1].replace(":","").rstrip('\n')
                         break
                
                conversion = (convert_from, convert_to, "1")
                conversion_strings.append(conversion)


            if "passedValue /" in lines[i]:
                for j in range(i, 0, -1):
                     if "switch" in lines[j]:
                         convert_from = lines[j-1].split(".")[1].replace(":","").rstrip('\n')
                         break

                convert_to = lines[i-1].split(".")[1].replace(":","").rstrip('\n')
                temp = lines[i]
                temp = temp.split("//")[0].split("Value /")[1].replace(";","")
                factor = "1/" + temp

                conversion = convert_from, convert_to, factor

                conversion_strings.append(conversion)

                

            if "passedValue *" in lines[i]:
                for j in range(i, 0, -1):
                     if "switch" in lines[j]:
                         convert_from = lines[j-1].split(".")[1].replace(":","").rstrip('\n')
                         break
                convert_to = lines[i-1].split(".")[1].replace(":","").rstrip('\n')
                temp = lines[i]
                factor = temp.split("//")[0].split("Value *")[1].replace(";","")

                conversion = convert_from, convert_to, factor

                conversion_strings.append(conversion)

        return conversion_strings