import os
from file_generators import *
from file_generator import *

def main():

    print("Insert name of unit class (e.g., Distance):", end= " ")
    raw_input = input()
    name = raw_input.capitalize()

    units = []
    units_plural = []

    while (raw_input != "0"):
        print("Insert name of unit (e.g. Foot) and enter 0 when done:", end= " ")
        raw_input = input()
        temp = raw_input.capitalize()
        if temp != "0":
            units.append(temp)

    for unit in units:
        print("Insert plural name of unit (e.g. Feet for Foot) for " + unit + ":", end= " ")
        raw_input = input()
        temp = raw_input.capitalize()
        units_plural.append(temp)

    conversion_factors = units_plural
    conversion_factors = [1]*len(conversion_factors)

    new_unit_class(name, units, units_plural, conversion_factors)


main()