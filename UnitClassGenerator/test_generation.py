# This will generate a unit called composite example, used to run tests on build server

from unit_class_reader import *
from composite_unit_class_generator import *

f2 = open("Tests\\DistanceProperties.cs")
f1 = open("Tests\\ForceProperties.cs")

unit_class_generator.generate("CompositeExample", f1, f2, "multiply")