from unit_class_reader import *
from composite_names_generator import *
from file_generator import *

class unit_class_generator():
    def generate(name, file_1, file_2, type):

        #file = open("DistanceProperties.cs")
        name_1, units, units_plural = unit_class_reader.read_properties(file_1)

        #file_2 = open("ForceProperties.cs")
        name_2, units_2, units_2_plural = unit_class_reader.read_properties(file_2)

        new_units, new_units_plural = names_generator.generate(units, units_plural, units_2, units_2_plural, type)

        sp = []

        conversion_factors = 1* len(new_units)

        print (new_units[0])
        print (new_units_plural[0])

        new_composite_unit_classes(name, new_units, new_units_plural, new_units, new_units_plural, sp, conversion_factors, name_1, name_2, type, units, units_2)

