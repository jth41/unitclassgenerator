import os
from file_generators import *

def new_unit_class(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors):
    if not os.path.exists(name): os.makedirs(name)
    
    file_methods = main_file.main_file.create, constants.constants.create, conversion.conversion.create, enums.enums.create, equality.equality.create, icomparable.icomparable.create, overloads.overloads.create, properties.properties.create, public_methods.public_methods.create, static_generators.static_generators.create, tests.tests.create
    for method in file_methods:
        method(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors)

def new_composite_unit_classes(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors, name_1, name_2, type, old_units, old_units_2):
    if not os.path.exists(name): os.makedirs(name)

    #file_methods = main_file.main_file.create, constants.constants.create, enums.enums.create, equality.equality.create, icomparable.icomparable.create, overloads.overloads.create, properties.properties.create, public_methods.public_methods.create, static_generators.static_generators.create
    main_file.main_file.create_composite(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors, name_1, name_2, old_units, old_units_2)
    constants.constants.create(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors)
    enums.enums.create(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors)
    equality.equality.create(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors)
    icomparable.icomparable.create_composite(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors)
    overloads.overloads.create_composite(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors, name_1, name_2)
    if type == "multiply":
        properties.properties.create_composite(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors, name_1, name_2)
    else:
        properties.properties.create_composite_divide(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors, name_1, name_2)
    public_methods.public_methods.create_composite(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors, name_1, name_2)
    static_generators.static_generators.create(name, units, units_plural, enum_names, enums_plural, special_constants, conversion_factors)
